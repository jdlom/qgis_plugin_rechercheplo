# RecherchePLO

## Plugin QGis

Extension servant aux gestionnaires de voirie (DIR) pour localiser un point sur leur réseau

## Démonstration

![Démonstration](demo.gif)

## Installation

* Télécharger l'archive puis extraire l'archive
* Renommer le dossier extrait par **recherchePLO** et copier le dans le répertoire des plugins de qgis.

Pour trouver le répertoire des plugins de qgis, depuis QGIS :
Préférences > Profils utilisateurs > Ouvrir le dossier actif puis aller dans python > plugins



## Prérequis

Disposer du RIU (Référentiel inter-urbain) ou du VSMAP en shp

Le nom des colonnes doivent être les suivantes pour le RIU:
 - gestionnai (Gestionnaire)
 - route (Numéro de route)
 - nom_plo_in (Numéro de PLO ou PR)

## Migration QGIS 3

 * 26 septembre 2020