# -*- coding: utf-8 -*-
"""
/***************************************************************************
 recherchePLO
                                 A QGIS plugin
 permet de localiser le PLO le plus proche
                              -------------------
        begin                : 2015-07-28
        git sha              : $Format:%H$
        copyright            : (C) 2015 by DIRNO/SPT/PEGR
        email                : Pegr.Spt.Dir-No@developpement-durable.gouv.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
from PyQt5.QtCore import QSettings, QTranslator, qVersion, QCoreApplication
from PyQt5.QtGui import  QIcon
from PyQt5.QtWidgets import QAction,QMessageBox, QFileDialog

from .recherchePlo_dialog import recherchePLODialog
import os.path
from qgis.gui import *
from qgis.core import *

class recherchePLO:
    """QGIS Plugin Implementation."""

    def __init__(self, iface):
        """Constructor.

        :param iface: An interface instance that will be passed to this class
            which provides the hook by which you can manipulate the QGIS
            application at run time.
        :type iface: QgsInterface
        """
        # Save reference to the QGIS interface
        self.iface = iface
        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)
        # initialize locale
        locale = QSettings().value('locale/userLocale')[0:2]
        locale_path = os.path.join(
            self.plugin_dir,
            'i18n',
            'recherchePLO_{}.qm'.format(locale))

        if os.path.exists(locale_path):
            self.translator = QTranslator()
            self.translator.load(locale_path)

            if qVersion() > '4.3.3':
                QCoreApplication.installTranslator(self.translator)

        # Create the dialog (after translation) and keep reference
        self.dlg = recherchePLODialog(iface)
        self.dlg.PLOButton.clicked.connect(self.giveCoordAtClick)
        self.dlg.pushButtonVsmap.clicked.connect(self.chargerVsmap)
        self.inclureEchangeurs = False
        self.dlg.checkBoxEchangeurs.stateChanged[int].connect(self.updateInclureEchangeurs)


        # Declare instance attributes
        self.actions = []
        self.menu = self.tr(u'&recherche PLO')
        # TODO: We are going to let the user set this up in a future iteration
        self.toolbar = self.iface.addToolBar(u'recherchePLO')
        self.toolbar.setObjectName(u'recherchePLO')

        #a reference to our ma canvas
        self.canvas = self.iface.mapCanvas()
        self.clickTool = QgsMapToolEmitPoint(self.canvas)
        self.clickTool.canvasClicked.connect(self.handleMouseDown)
        #self.dlg.connectCheckBox(self.afficher)
        self.point = None 

        #display a message in order to choose the vsmap on the firstuse
        self.settings = QSettings()
        s = self.settings
        firstUse = s.value("recherchePLO/isFirstUse" , 1, type=int)
        if firstUse == 1:
            self.dlg.tabWidget.setCurrentIndex(1) #bascule sur l'onglet 1 (configuration)
            s.setValue("recherchePLO/isFirstUse", 0)
            QMessageBox.information( self.iface.mainWindow(),"Configuration VSMAP", "Veuillez charger le VSMAP avant l'utilisation dans l'onglet configuration")

        else :
                self.gestionnaire = s.value("recherchePLO/gestionnaire","")
                vsmap_charge = self.loadVSMAP()
                if vsmap_charge :
                    self.inialiseCombobox(self.gestionnaire)
                    self.initialiseReseau()
                
    def afficher(self,etat):
        QMessageBox.information( self.iface.mainWindow(),"Etat", "X= %s" % (str(etat)))
    
    def updateInclureEchangeurs(self, etat):
        if etat == 0:
            self.inclureEchangeurs = False
        else:
            self.inclureEchangeurs = True
        self.initialiseReseau(self.inclureEchangeurs)

    def inialiseCombobox(self,gestionnaire):
        liste = set()
        liste.add('')
        reseau = self.reseau
        #key = 'Gestionnaire'
        key = 'gestionnai'
        for f in reseau.getFeatures():
            liste.add(f[key])
        listeGestionnaire = list(liste)
        self.dlg.comboBoxGestionnaire.clear()
        self.dlg.comboBoxGestionnaire.addItems(listeGestionnaire)
        if gestionnaire not in listeGestionnaire:
            self.dlg.textEditConfigLog.setText("Attention : gestionnaire %s introuvable dans le VSMAP présent" % (str(gestionnaire)))
        else:
            index = self.dlg.comboBoxGestionnaire.findText(gestionnaire)
            self.dlg.comboBoxGestionnaire.setCurrentIndex(index)
        self.dlg.comboBoxGestionnaire.currentIndexChanged[str].connect(self.filtrerGestionnaire)

    def loadVSMAP(self):
        try:
            s = self.settings
            vsmapPath = s.value("recherchePLO/vsmapPath")
            if not os.path.isfile(vsmapPath):
                self.dlg.labelPathVsmap.setText(u'Non chargé ou introuvable')
                return
            label = '../'+'/'.join(vsmapPath.split('/')[-2:])
            self.dlg.labelPathVsmap.setText(label)
            self.dlg.labelPathVsmap.setToolTip(vsmapPath)
            self.reseau = QgsVectorLayer(vsmapPath, "recherchePLOVsmap", "ogr")
            return True
        except:
            return False

    def initialiseReseau(self, inclureEchangeurs=False):
        try:
            reseau = self.reseau
            gestionnaire = self.gestionnaire
            if reseau.isValid():
                if gestionnaire != '':
                    sql = "gestionnai = '%s'" % (str(gestionnaire))
                #QgsMessageLog.logMessage("reseau", "name")
                if not inclureEchangeurs:
                    if gestionnaire != '':
                        sql = sql + " AND NOT nom_plo_in LIKE 'DB%'"
                    else : 
                        sql = "NOT nom_plo_in LIKE 'DB%'"
                #QMessageBox.information( self.iface.mainWindow(),"Info sql", "sql= %s" % (str(sql)))
                if sql == '':
                    pass
                else :
                    reseau.dataProvider().setSubsetString(sql)
                self.allReseauFeatures = {feature.id(): feature for feature in reseau.getFeatures()}
                #build index spatial on reseau
                self.index = QgsSpatialIndex()
                self.index.addFeatures(self.allReseauFeatures.values())
                #map(self.index.insertFeature, self.allReseauFeatures.values())

        except:
            pass
    # noinspection PyMethodMayBeStatic
    def tr(self, message):
        """Get the translation for a string using Qt translation API.

        We implement this ourselves since we do not inherit QObject.

        :param message: String for translation.
        :type message: str, QString

        :returns: Translated version of message.
        :rtype: QString
        """
        # noinspection PyTypeChecker,PyArgumentList,PyCallByClass
        return QCoreApplication.translate('recherchePLO', message)


    def add_action(
        self,
        icon_path,
        text,
        callback,
        enabled_flag=True,
        add_to_menu=True,
        add_to_toolbar=True,
        status_tip=None,
        whats_this=None,
        parent=None):
        """Add a toolbar icon to the toolbar.

        :param icon_path: Path to the icon for this action. Can be a resource
            path (e.g. ':/plugins/foo/bar.png') or a normal file system path.
        :type icon_path: str

        :param text: Text that should be shown in menu items for this action.
        :type text: str

        :param callback: Function to be called when the action is triggered.
        :type callback: function

        :param enabled_flag: A flag indicating if the action should be enabled
            by default. Defaults to True.
        :type enabled_flag: bool

        :param add_to_menu: Flag indicating whether the action should also
            be added to the menu. Defaults to True.
        :type add_to_menu: bool

        :param add_to_toolbar: Flag indicating whether the action should also
            be added to the toolbar. Defaults to True.
        :type add_to_toolbar: bool

        :param status_tip: Optional text to show in a popup when mouse pointer
            hovers over the action.
        :type status_tip: str

        :param parent: Parent widget for the new action. Defaults None.
        :type parent: QWidget

        :param whats_this: Optional text to show in the status bar when the
            mouse pointer hovers over the action.

        :returns: The action that was created. Note that the action is also
            added to self.actions list.
        :rtype: QAction
        """

        icon = QIcon(icon_path)
        action = QAction(icon, text, parent)
        action.triggered.connect(callback)
        action.setEnabled(enabled_flag)

        if status_tip is not None:
            action.setStatusTip(status_tip)

        if whats_this is not None:
            action.setWhatsThis(whats_this)

        if add_to_toolbar:
            self.toolbar.addAction(action)

        if add_to_menu:
            self.iface.addPluginToMenu(
                self.menu,
                action)

        self.actions.append(action)

        return action

    def initGui(self):
        """Create the menu entries and toolbar icons inside the QGIS GUI."""

        icon_path = os.path.join(os.path.dirname(__file__),'icon.png')
        self.add_action(
            icon_path,
            text=self.tr(u'localise le PLO'),
            callback=self.run,
            parent=self.iface.mainWindow())


    def unload(self):
        """Removes the plugin menu item and icon from QGIS GUI."""
        for action in self.actions:
            self.iface.removePluginMenu(
                self.tr(u'&recherche PLO'),
                action)
            self.iface.removeToolBarIcon(action)
        # remove the toolbar
        del self.toolbar


    def handleMouseDown(self, point, button):
        self.point = QgsPointXY(point)
        lineId = self.lineIdwithMinDistance(point,self.index.nearestNeighbor(point,2))
        if lineId == "bad precision":
            self.dlg.resetLineEdit()
            self.dlg.updateLineEdit(self.dlg.textEditLog,"Erreur: impossible de localiser le PLO")
        else:
            self.dlg.resetLineEdit()
        #updateLineEdit(lineEdit à éditer, champs que l'on récupère)
            self.dlg.updateLineEdit(self.dlg.lineEditRoute,str(self.allReseauFeatures[lineId]['route']))
            self.dlg.updateLineEdit(self.dlg.lineEditPlo,str(self.allReseauFeatures[lineId]['nom_plo_in']))
            self.dlg.updateLineEdit(self.dlg.lineEditGestionnaire,str(self.allReseauFeatures[lineId]['gestionnai']))

            abscisse = int(self.allReseauFeatures[lineId].geometry().lineLocatePoint(
                QgsGeometry.fromPointXY(point)))
            
            self.dlg.updateLineEdit(self.dlg.lineEditAbscisse,str(abscisse))


    def giveCoordAtClick(self):
        self.canvas.setMapTool(self.clickTool)

    def lineIdwithMinDistance(self, point,lineIdList):
      ''' Return the lineId with the shorstest distance to the point, because Index.nearestNeighbor return an unordered list of id
      '''
      allreseaufeatures=self.allReseauFeatures
      min = float("inf")
      geom = QgsGeometry.fromPointXY(point)
      for f in lineIdList:
        if geom.distance(allreseaufeatures[f].geometry()) < min :
          closestLineId = f
          min = geom.distance(allreseaufeatures[f].geometry())
      return closestLineId

    def chargerVsmap(self):
        filename = QFileDialog.getOpenFileName(self.iface.mainWindow(),"Ouvrir VSMAP",'', "VSMAP (*.shp)")
        if filename:
            s = self.settings
            s.setValue("recherchePLO/vsmapPath",filename[0])
            vsmap_charge = self.loadVSMAP()
            if vsmap_charge:
                self.inialiseCombobox('')
                self.initialiseReseau(self.inclureEchangeurs)

    def filtrerGestionnaire(self,gestionnaire):
        s = self.settings
        self.gestionnaire = gestionnaire
        s.setValue("recherchePLO/gestionnaire",gestionnaire)
        self.initialiseReseau(self.inclureEchangeurs)

    def run(self):
        """Run method that performs all the real work"""
        # show the dialog
        if self.dlg.isHidden():
            self.dlg.show()
            self.dlg.resetLineEdit()
        else:
            self.dlg.hide()



