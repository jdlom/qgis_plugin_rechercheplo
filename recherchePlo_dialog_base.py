# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'dockwidget.ui'
#
# Created: Fri Jul 31 16:20:17 2015
#      by: PyQt4 UI code generator 4.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtWidgets, QtGui
import os 
try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtWidgets.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtWidgets.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtWidgets.QApplication.translate(context, text, disambig)

class Ui_recherchePLOForm(object):
    def setupUi(self, recherchePLOForm):
        recherchePLOForm.setObjectName(_fromUtf8("recherchePLOForm"))
        recherchePLOForm.resize(324, 417)
        self.dockWidgetContents = QtWidgets.QWidget()
        self.dockWidgetContents.setObjectName(_fromUtf8("dockWidgetContents"))
        self.verticalLayout_5 = QtWidgets.QVBoxLayout(self.dockWidgetContents)
        self.verticalLayout_5.setObjectName(_fromUtf8("verticalLayout_5"))
        self.scrollArea = QtWidgets.QScrollArea(self.dockWidgetContents)
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setObjectName(_fromUtf8("scrollArea"))
        self.scrollAreaWidgetContents = QtWidgets.QWidget()
        self.scrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 304, 375))
        self.scrollAreaWidgetContents.setObjectName(_fromUtf8("scrollAreaWidgetContents"))
        self.verticalLayout_6 = QtWidgets.QVBoxLayout(self.scrollAreaWidgetContents)
        self.verticalLayout_6.setObjectName(_fromUtf8("verticalLayout_6"))
        self.tabWidget = QtWidgets.QTabWidget(self.scrollAreaWidgetContents)
        self.tabWidget.setObjectName(_fromUtf8("tabWidget"))
        self.tab = QtWidgets.QWidget()
        self.tab.setObjectName(_fromUtf8("tab"))
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.tab)
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.PLOButton = QtWidgets.QPushButton(self.tab)
        self.PLOButton.setText(_fromUtf8(""))
        icon = QtGui.QIcon()
        search_icon_path = os.path.join(os.path.dirname(__file__),'icons','search.png')
        icon.addPixmap(QtGui.QPixmap(search_icon_path), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.PLOButton.setIcon(icon)
        self.PLOButton.setObjectName(_fromUtf8("PLOButton"))
        self.verticalLayout.addWidget(self.PLOButton)
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.lineEditGestionnaire = QtWidgets.QLineEdit(self.tab)
        self.lineEditGestionnaire.setObjectName(_fromUtf8("lineEditGestionnaire"))
        self.gridLayout.addWidget(self.lineEditGestionnaire, 3, 1, 1, 1)
        self.labelAbscisse = QtWidgets.QLabel(self.tab)
        self.labelAbscisse.setObjectName(_fromUtf8("labelAbscisse"))
        self.gridLayout.addWidget(self.labelAbscisse, 2, 0, 1, 1)
        self.labelRoute = QtWidgets.QLabel(self.tab)
        self.labelRoute.setObjectName(_fromUtf8("labelRoute"))
        self.gridLayout.addWidget(self.labelRoute, 0, 0, 1, 1)
        self.labelPlo = QtWidgets.QLabel(self.tab)
        self.labelPlo.setObjectName(_fromUtf8("labelPlo"))
        self.gridLayout.addWidget(self.labelPlo, 1, 0, 1, 1)
        self.lineEditRoute = QtWidgets.QLineEdit(self.tab)
        self.lineEditRoute.setObjectName(_fromUtf8("lineEditRoute"))
        self.gridLayout.addWidget(self.lineEditRoute, 0, 1, 1, 1)
        self.lineEditPlo = QtWidgets.QLineEdit(self.tab)
        self.lineEditPlo.setObjectName(_fromUtf8("lineEditPlo"))
        self.gridLayout.addWidget(self.lineEditPlo, 1, 1, 1, 1)
        self.lineEditAbscisse = QtWidgets.QLineEdit(self.tab)
        self.lineEditAbscisse.setObjectName(_fromUtf8("lineEditAbscisse"))
        self.gridLayout.addWidget(self.lineEditAbscisse, 2, 1, 1, 1)
        self.labelGestionnaire = QtWidgets.QLabel(self.tab)
        self.labelGestionnaire.setObjectName(_fromUtf8("labelGestionnaire"))
        self.gridLayout.addWidget(self.labelGestionnaire, 3, 0, 1, 1)
        self.verticalLayout.addLayout(self.gridLayout)
        self.checkBoxEchangeurs = QtWidgets.QCheckBox(self.tab)
        self.checkBoxEchangeurs.setObjectName(_fromUtf8("checkBoxEchangeurs"))
        self.verticalLayout.addWidget(self.checkBoxEchangeurs)
        self.textEditLog = QtWidgets.QTextEdit(self.tab)
        self.textEditLog.setMinimumSize(QtCore.QSize(0, 0))
        self.textEditLog.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.textEditLog.setObjectName(_fromUtf8("textEditLog"))
        self.verticalLayout.addWidget(self.textEditLog)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)
        self.verticalLayout_2.addLayout(self.verticalLayout)
        self.tabWidget.addTab(self.tab, _fromUtf8(""))
        self.tab_2 = QtWidgets.QWidget()
        self.tab_2.setObjectName(_fromUtf8("tab_2"))
        self.verticalLayout_7 = QtWidgets.QVBoxLayout(self.tab_2)
        self.verticalLayout_7.setObjectName(_fromUtf8("verticalLayout_7"))
        self.verticalLayout_3 = QtWidgets.QVBoxLayout()
        self.verticalLayout_3.setObjectName(_fromUtf8("verticalLayout_3"))
        self.gridLayout_2 = QtWidgets.QGridLayout()
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        self.labelPathVsmap = QtWidgets.QLabel(self.tab_2)
        self.labelPathVsmap.setToolTip(_fromUtf8(""))
        self.labelPathVsmap.setObjectName(_fromUtf8("labelPathVsmap"))
        self.gridLayout_2.addWidget(self.labelPathVsmap, 0, 1, 1, 1)
        self.labelVSMAP = QtWidgets.QLabel(self.tab_2)
        self.labelVSMAP.setObjectName(_fromUtf8("labelVSMAP"))
        self.gridLayout_2.addWidget(self.labelVSMAP, 0, 0, 1, 1)
        self.labelGestionnaire_2 = QtWidgets.QLabel(self.tab_2)
        self.labelGestionnaire_2.setObjectName(_fromUtf8("labelGestionnaire_2"))
        self.gridLayout_2.addWidget(self.labelGestionnaire_2, 1, 0, 1, 1)
        self.comboBoxGestionnaire = QtWidgets.QComboBox(self.tab_2)
        self.comboBoxGestionnaire.setObjectName(_fromUtf8("comboBoxGestionnaire"))
        self.gridLayout_2.addWidget(self.comboBoxGestionnaire, 1, 1, 1, 1)
        self.verticalLayout_3.addLayout(self.gridLayout_2)
        self.pushButtonVsmap = QtWidgets.QPushButton(self.tab_2)
        self.pushButtonVsmap.setObjectName(_fromUtf8("pushButtonVsmap"))
        self.verticalLayout_3.addWidget(self.pushButtonVsmap)
        self.textEditConfigLog = QtWidgets.QTextEdit(self.tab_2)
        self.textEditConfigLog.setObjectName(_fromUtf8("textEditConfigLog"))
        self.verticalLayout_3.addWidget(self.textEditConfigLog)
        spacerItem1 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_3.addItem(spacerItem1)
        self.verticalLayout_7.addLayout(self.verticalLayout_3)
        self.tabWidget.addTab(self.tab_2, _fromUtf8(""))
        self.verticalLayout_6.addWidget(self.tabWidget)
        self.scrollArea.setWidget(self.scrollAreaWidgetContents)
        self.verticalLayout_5.addWidget(self.scrollArea)
        recherchePLOForm.setWidget(self.dockWidgetContents)

        self.retranslateUi(recherchePLOForm)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(recherchePLOForm)

    def retranslateUi(self, recherchePLOForm):
        recherchePLOForm.setWindowTitle(_translate("recherchePLOForm", "Recherche PLO - outils de recherche", None))
        self.labelAbscisse.setText(_translate("recherchePLOForm", "Abscisse", None))
        self.labelRoute.setText(_translate("recherchePLOForm", "Route", None))
        self.labelPlo.setText(_translate("recherchePLOForm", "PLO", None))
        self.labelGestionnaire.setText(_translate("recherchePLOForm", "Gestionnaire", None))
        self.checkBoxEchangeurs.setText(_translate("recherchePLOForm", "Inclure les échangeurs", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab), _translate("recherchePLOForm", "Recherche PLO", None))
        self.labelPathVsmap.setText(_translate("recherchePLOForm", "Aucun", None))
        self.labelVSMAP.setText(_translate("recherchePLOForm", "VSMAP", None))
        self.labelGestionnaire_2.setText(_translate("recherchePLOForm", "Gestionnaire", None))
        self.pushButtonVsmap.setText(_translate("recherchePLOForm", "Charger un nouveau fichier VSMAP", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_2), _translate("recherchePLOForm", "Configuration", None))

#import resources
