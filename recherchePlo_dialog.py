# -*- coding: utf-8 -*-
"""
/***************************************************************************
 recherchePLODialog
                                 A QGIS plugin
 permet de localiser le PLO le plus proche
                             -------------------
        begin                : 2015-07-28
        git sha              : $Format:%H$
        copyright            : (C) 2015 by DIRNO/SPT/PEGR
        email                : Pegr.Spt.Dir-No@developpement-durable.gouv.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import os

from PyQt5 import QtWidgets 
from PyQt5 import QtCore
from .recherchePlo_dialog_base import *

class recherchePLODialog(QtWidgets.QDockWidget, Ui_recherchePLOForm):
    def __init__(self, iface):
        """Constructor."""
        QtWidgets.QDockWidget.__init__(self)
        self.iface = iface

        # Set up the user interface from Designer.
        # After setupUI you can access any designer object by doing
        # self.<objectname>, and you can use autoconnect slots - see
        # http://qt-project.org/doc/qt-4.8/designer-using-a-ui-file.html
        # #widgets-and-dialogs-with-auto-connect
        self.setupUi(self)
        self.iface.addDockWidget(QtCore.Qt.RightDockWidgetArea, self)

    def connectButtonOnClick(self, boutton, methode):
        boutton.clicked.connect(methode)

    def connectCheckBox(self,methode):
        self.checkBoxEchangeurs.stateChanged.connect(methode)




    def updateLineEdit(self,lineEditName,string):
        lineEditName.setText(string)

    def resetLineEdit(self):
        self.lineEditRoute.clear()
        self.lineEditPlo.clear()
        self.lineEditAbscisse.clear()
        self.lineEditGestionnaire.clear()
        self.textEditLog.clear()


